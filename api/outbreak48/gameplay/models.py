from django.db import models

class Game(models.Model):
	name = modes.CharField(max_length = 50)
	start = models.DateTimeField()
	end = models.DateTimeField(null = True)
	public = models.BooleanField(default = True)
	incubation = models.PositiveIntegerField(default = 48)
	
	def __unicode__(self):
		return self.name
	
	class Meta:
		ordering = ('-start',)
		get_latest_by = 'start'

class Role(models.Model):
	name = models.CharField(max_length = 20)
	
	def __unicode__(self):
		return self.name
	
	class Meta:
		ordering = ('name',)

class Mission(models.Model)::
	game = models.ForeignKey(Game, related_name = 'missions')
	name = models.CharFeld(max_length = 50)
	roles = models.ManyToManyField(Role, related_name = 'missions')
	
	def __unicode__(self):
		return self.name

class Task(models.Model):
	mission = models.ForeignKey(Mission, related_name = 'tasks')
	name = models.CharFeld(max_length = 50)
	
	def __unicode__(self):
		return self.name

class Player(models.Model):
	game = models.ForeignKey(Player, related_name = 'players')
	user = models.ForeignKey('auth.User', related_name = 'players')
	token = models.CharField(u'Facebook token', max_length = 50)
	infected = models.DateTimeField(null = True)
	roles = models.ManyToManyField(Role, related_name = 'players')
	missions = models.ManyToManyField(Mission, through = 'PlayerMission',
		related_name = 'players'
	)
	
	def __unicode__(self):
		return u'%s playing %s' % (
			self.user.get_full_name() or self.user.username,
			self.game.name
		)
	
	class Meta:
		unique_together = ('game', 'facebook_id')

class PayerMission(models.Model):
	player = models.ForeignKey(Player, related_name = 'missions')
	mission = models.ForeignKey(Mission)
	started = models.DateTimeField(auto_now_add = True)
	completed = models.DateTimeField(null = True)