from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	# url(r'^$', 'outbreak48.views.home', name='home'),
	# url(r'^outbreak48/', include('outbreak48.foo.urls')),
	url(r'^admin/', include(admin.site.urls))
)
