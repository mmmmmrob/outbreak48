# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Iteration'
        db.create_table('gameplay_iteration', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('start', self.gf('django.db.models.fields.DateTimeField')()),
            ('length', self.gf('django.db.models.fields.PositiveIntegerField')(default=48)),
        ))
        db.send_create_signal('gameplay', ['Iteration'])

        # Adding model 'Player'
        db.create_table('gameplay_player', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('iteration', self.gf('django.db.models.fields.related.ForeignKey')(related_name='players', to=orm['gameplay.Iteration'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='players', to=orm['auth.User'])),
        ))
        db.send_create_signal('gameplay', ['Player'])

        # Adding unique constraint on 'Player', fields ['iteration', 'user']
        db.create_unique('gameplay_player', ['iteration_id', 'user_id'])

        # Adding M2M table for field meetups on 'Player'
        db.create_table('gameplay_player_meetups', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('player', models.ForeignKey(orm['gameplay.player'], null=False)),
            ('meetup', models.ForeignKey(orm['gameplay.meetup'], null=False))
        ))
        db.create_unique('gameplay_player_meetups', ['player_id', 'meetup_id'])

        # Adding model 'PlayerTask'
        db.create_table('gemaplay_player_tasks', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('player', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gameplay.Player'])),
            ('task', self.gf('django.db.models.fields.related.ForeignKey')(related_name='players', to=orm['gameplay.Task'])),
            ('completed', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal('gameplay', ['PlayerTask'])

        # Adding model 'Task'
        db.create_table('gameplay_task', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('iteration', self.gf('django.db.models.fields.related.ForeignKey')(related_name='tasks', to=orm['gameplay.Iteration'])),
            ('latitude', self.gf('django.db.models.fields.FloatField')()),
            ('longitude', self.gf('django.db.models.fields.FloatField')()),
            ('backstory', self.gf('django.db.models.fields.TextField')()),
            ('endstory', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('gameplay', ['Task'])

        # Adding model 'Meetup'
        db.create_table('gameplay_meetup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('iteration', self.gf('django.db.models.fields.related.ForeignKey')(related_name='meetups', to=orm['gameplay.Iteration'])),
            ('latitude', self.gf('django.db.models.fields.FloatField')()),
            ('longitude', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal('gameplay', ['Meetup'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'Player', fields ['iteration', 'user']
        db.delete_unique('gameplay_player', ['iteration_id', 'user_id'])

        # Deleting model 'Iteration'
        db.delete_table('gameplay_iteration')

        # Deleting model 'Player'
        db.delete_table('gameplay_player')

        # Removing M2M table for field meetups on 'Player'
        db.delete_table('gameplay_player_meetups')

        # Deleting model 'PlayerTask'
        db.delete_table('gemaplay_player_tasks')

        # Deleting model 'Task'
        db.delete_table('gameplay_task')

        # Deleting model 'Meetup'
        db.delete_table('gameplay_meetup')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'gameplay.iteration': {
            'Meta': {'ordering': "('-start',)", 'object_name': 'Iteration'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.PositiveIntegerField', [], {'default': '48'}),
            'start': ('django.db.models.fields.DateTimeField', [], {})
        },
        'gameplay.meetup': {
            'Meta': {'object_name': 'Meetup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iteration': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'meetups'", 'to': "orm['gameplay.Iteration']"}),
            'latitude': ('django.db.models.fields.FloatField', [], {}),
            'longitude': ('django.db.models.fields.FloatField', [], {})
        },
        'gameplay.player': {
            'Meta': {'unique_together': "(('iteration', 'user'),)", 'object_name': 'Player'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iteration': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'players'", 'to': "orm['gameplay.Iteration']"}),
            'meetups': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'players'", 'symmetrical': 'False', 'to': "orm['gameplay.Meetup']"}),
            'tasks': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['gameplay.Task']", 'through': "orm['gameplay.PlayerTask']", 'symmetrical': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'players'", 'to': "orm['auth.User']"})
        },
        'gameplay.playertask': {
            'Meta': {'object_name': 'PlayerTask', 'db_table': "'gemaplay_player_tasks'"},
            'completed': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gameplay.Player']"}),
            'task': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'players'", 'to': "orm['gameplay.Task']"})
        },
        'gameplay.task': {
            'Meta': {'object_name': 'Task'},
            'backstory': ('django.db.models.fields.TextField', [], {}),
            'endstory': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iteration': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tasks'", 'to': "orm['gameplay.Iteration']"}),
            'latitude': ('django.db.models.fields.FloatField', [], {}),
            'longitude': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['gameplay']
