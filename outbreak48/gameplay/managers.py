from django.db.models import Manager

class TaskManager(Manager):
	def get_query_set(self):
		return self.model.QuerySet(self.model)
		
	def near(self, latitude, longitude):
		return self.get_query_set().near(latitude, longitude)

class MeetupManager(Manager):
	def get_query_set(self):
		return self.model.QuerySet(self.model)
		
	def near(self, latitude, longitude):
		return self.get_query_set().near(latitude, longitude)