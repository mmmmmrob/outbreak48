from django.db import models
from django.conf import settings
from outbreak48.gameplay.managers import TaskManager, MeetupManager
from datetime import datetime

class Iteration(models.Model):
	start = models.DateTimeField()
	length = models.PositiveIntegerField(default = 48)
	
	def __unicode__(self):
		return self.start.strftime('%d %A %Y')
	
	class Meta:
		ordering = ('-start',)
		get_latest_by = 'start'

class Player(models.Model):
	iteration = models.ForeignKey(Iteration, related_name = 'players')
	user = models.ForeignKey('auth.User', related_name = 'players')
	tasks = models.ManyToManyField('Task', through = 'PlayerTask')
	meetups = models.ManyToManyField('Meetup',
		related_name = 'players', null = True, blank = True
	)
	infected = models.DateTimeField(null = True, blank = True)
	latitude = models.FloatField(null = True)
	longitude = models.FloatField(null = True)
	
	def nearby_tasks(self, latitude, longitude):
		tasks = self.iteration.tasks.filter(
			players__isnull = True
		).near(latitude, longitude)
		
		maximum = settings.TASKS_PER_ITERATION
		tasks = tasks.order_by('?')[:maximum]
		
		return tasks
	
	class Meta:
		unique_together = ('iteration', 'user')

class PlayerTask(models.Model):
	player = models.ForeignKey('Player')
	task = models.ForeignKey('Task', related_name = 'players')
	completed = models.DateTimeField(null = True, blank = True)
	
	class Meta:
		db_table = 'gemaplay_player_tasks'

class Task(models.Model):
	iteration = models.ForeignKey(Iteration, related_name = 'tasks')
	name = models.CharField(max_length = 50)
	latitude = models.FloatField()
	longitude = models.FloatField()
	backstory = models.TextField()
	endstory = models.TextField()
	objects = TaskManager()
	
	def __unicode__(self):
		return self.name
	
	def can_complete(self, latitude, longitude):
		from geopy.distance import distance
		from geopy.point import Point
		
		minimum = settings.MIN_TASK_DISTANCE
		source = Point(self.latitude, self.longitude)
		dest = Point(latitude, longitude)
		distance = distance(source, dest).miles
		return distance <= minimum
	
	def complete(self, player):
		return PlayerTask.objects.create(
			player = player,
			task = self,
			completed = datetime.now()
		)
	
	class QuerySet(models.query.QuerySet):
		def near(self, latitude, longitude):
			sql = """((ACOS(SIN(%(latitude)s * PI() /
			180) * SIN(`%(model)s`.`latitude` * PI() / 180) + COS(%(latitude)s * PI() / 180) *
			COS(`%(model)s`.`latitude` * PI() / 180) * COS((%(longitude)s - 
			`%(model)s`.`longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)""" % {
				'latitude': latitude,
				'longitude': longitude,
				'model': self.model._meta.db_table,
			}
			
			return self.extra(
				select = {
					'proximity': sql
				}
			).extra(
				where = [
					'%s <= %d' % (sql, settings.MAX_TASK_RAIUS)
				]
			)

class Meetup(models.Model):
	iteration = models.ForeignKey(Iteration, related_name = 'meetups')
	latitude = models.FloatField()
	longitude = models.FloatField()
	objects = MeetupManager()

	class QuerySet(models.query.QuerySet):
		def near(self, latitude, longitude):
			sql = """((ACOS(SIN(%(latitude)s * PI() /
			180) * SIN(`%(model)s`.`latitude` * PI() / 180) + COS(%(latitude)s * PI() / 180) *
			COS(`%(model)s`.`latitude` * PI() / 180) * COS((%(longitude)s - 
			`%(model)s`.`longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515)""" % {
				'latitude': latitude,
				'longitude': longitude,
				'model': self.model._meta.db_table,
			}
			
			return self.extra(
				select = {
					'proximity': sql
				}
			).extra(
				where = [
					'%s <= %d' % (sql, settings.MAX_TASK_RAIUS)
				]
			)

	def can_cure(self, latitude, longitude):
		from geopy.distance import distance
		from geopy.point import Point
		
		minimum = settings.MIN_TASK_DISTANCE
		source = Point(self.latitude, self.longitude)
		dest = Point(latitude, longitude)
		distance = distance(source, dest).miles
		return distance <= minimum
