from django.contrib import admin
from outbreak48.gameplay.models import *

admin.site.register(Iteration)
admin.site.register(Player)
admin.site.register(PlayerTask)
admin.site.register(Task)
admin.site.register(Meetup)