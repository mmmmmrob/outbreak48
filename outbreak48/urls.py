from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.conf import settings
from utils import add_registered_urls
from views import *

admin.autodiscover()

urlpatterns = patterns('',
	# url(r'^$', home_route),
	# url(r'^outbreak48/', include('outbreak48.foo.urls')),

	url(r'^admin/', include(admin.site.urls)),
	url(r'^accounts/', include('registration.urls')),
	url(r'^pickup/(?P<task>\d+)/$', 'outbreak48.views.pickup'),
	# redirects
	#('^foo/(?P<id>\d+)/$', redirect_to, {'url': '/bar/%(id)s/'}),
)

add_registered_urls(urlpatterns)

urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )
