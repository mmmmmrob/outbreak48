from settings_local import *
from os import path

TEMPLATE_DEBUG = DEBUG

ADMINS = (
	# ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS
TIME_ZONE = ''
LANGUAGE_CODE = 'en-gb'
SITE_ID = 1
USE_I18N = False
USE_L10N = True
SITE_ROOT = path.abspath(path.dirname(__file__) + '/../outbreak48')
MEDIA_ROOT = path.join(SITE_ROOT, 'media') + '/'
MEDIA_URL = '/media/'
STATIC_ROOT = path.join(SITE_ROOT, 'static') + '/'
STATIC_URL = '/static/'
ADMIN_MEDIA_PREFIX = '/static/admin/'

STATICFILES_DIRS = (
	# Put strings here, like "/home/html/static" or "C:/www/django/static".
	# Always use forward slashes, even on Windows.
	# Don't forget to use absolute paths, not relative paths.
)

STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder'
)

SECRET_KEY = '9frv&xy4-8@r%%+n4s$1hdps=p*d_*(bl2nrk(%jr)^x+#xhox'

TEMPLATE_LOADERS = (
	'django.template.loaders.filesystem.Loader',
	'django.template.loaders.app_directories.Loader'
)

MIDDLEWARE_CLASSES = (
	'django.middleware.common.CommonMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware'
)

ROOT_URLCONF = 'outbreak48.urls'

TEMPLATE_DIRS = (

     path.join(SITE_ROOT, 'templates'),
)

INSTALLED_APPS = (
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'django.contrib.admin',
	'south',
	'registration',
	'outbreak48.gameplay'
)

ACCOUNT_ACTIVATION_DAYS = 7

LOGIN_REDIRECT_URL = "/"

LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'handlers': {
		'mail_admins': {
			'level': 'ERROR',
			'class': 'django.utils.log.AdminEmailHandler'
		}
	},
	'loggers': {
		'django.request': {
			'handlers': ['mail_admins'],
			'level': 'ERROR',
			'propagate': True,
		},
	}
}

TASKS_PER_ITERATION = 5
MIN_TASK_DISTANCE = 1
MAX_TASK_RAIUS = 10