from django.shortcuts import HttpResponse, Http404, render_to_response
from django.template import RequestContext, loader, Context
from django.conf.urls.defaults import patterns, url
from functools import wraps
import string
from random import Random

def render(template_name, title=''):
    def inner_render(fn):
        def wrapped(request, *args, **kwargs):
        	d = fn(request, *args, **kwargs)
        	if 'title' not in d:
        		d['title'] = title
        	return render_to_response(template_name,
                                       d,
                                       context_instance = RequestContext(request))
        return wraps(fn)(wrapped)
    return inner_render

# --------------------------------------------------

regd_urls = dict()
class register_url(object):

    def __init__(self, url):
        regd_urls[url] = self

    def __call__(self, f):
        def wrapped_f(*args):
            return f(*args)
        self.wrap = wrapped_f
        return wrapped_f
    
    def get(self):
        return self.wrap

def add_registered_urls(urlpat):
    for k in regd_urls.keys():
        urlpat += patterns('', url(k, regd_urls[k].get()))
        
# --------------------------------------------------

def randomPass(length=32):
    password = ''
    letters = string.ascii_letters + string.digits
    r = Random()
    for n in range(0, length):
        index = r.randrange( len(letters) )
        password += letters[index]
    return password
