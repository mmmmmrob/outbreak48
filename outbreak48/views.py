from django.shortcuts import HttpResponse, HttpResponseRedirect, Http404, render_to_response
from django.template import RequestContext, loader, Context, TemplateDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from random import Random
from utils import render, register_url
from outbreak48.gameplay.models import *
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
import string

CURE_TIME = 10.0

@register_url(r'^$')
def home_route(request):

	d = dict()
	template = 'index.html';

	if request.user.is_authenticated():

		if not get_lat_lon(request):
			return HttpResponseRedirect('/locate/')

		d.update(get_nearest_tasks(request.user))	
		d.update(get_nearest_cure_centers(request.user))
		d.update(get_completed_tasks(request.user))	
		d.update({'is_infected' : get_is_infected(request.user)});

		template = 'dashboard.html'

	return render_to_response(template, d, context_instance = RequestContext(request))

@login_required
def pickup(request, task):

	#if not get_lat_lon(request):
	#	return HttpResponseRedirect('/locate/')

	d = dict()
	try:
		d['result'] = attempt_pickup(request.user, task)	
	except Task.DoesNotExist:
		return HttpResponseRedirect('/')
	
	return HttpResponseRedirect('/')

@register_url(r'^cure/$')
@login_required
def cure(request):

	#if not get_lat_lon(request):
	#	return HttpResponseRedirect('/locate/')

	d = dict()
	template = ''

	if request.method == 'POST':
		template = 'cure_outcome.html'
		d['cured'] = not get_is_infected(request.user)
	else:
		template = 'attempt_cure.html'
		attempt_cure(request.user)
	d['cure_time'] = CURE_TIME

	return render_to_response(template, d, context_instance = RequestContext(request))

@register_url(r'^locate/$')
@login_required
def locate(request):
	return render_to_response("locate.html", {}, context_instance = RequestContext(request))	

# ------------------------------------------------------------

def get_completed_tasks(user):
	iteration = Iteration.objects.latest()
	
	tasks = Task.objects.filter(
		iteration = iteration,
		players__player__user = user
	)

	# pad to 5
	taskList = [t for t in tasks]
	for x in range(len(tasks), 5):
		taskList.append('')
	
	return dict({'tasks' : taskList})

def attempt_pickup(user, task_id, latitude = None, longitude = None):
	player = Player.objects.get(
		user = user,
		iteration = Iteration.objects.latest()
	)
	
	if not latitude or not longitude:
		latitude, longitude = player.latitude, player.longitude
	
	task = Task.objects.get(
		pk = task_id,
		players__isnull = True
	)
	
	if task.can_complete(latitude, longitude):
		task.complete(player)
		return task
	
	return None

def get_is_infected(user):
	player = Player.objects.get(
		user = user,
		iteration = Iteration.objects.latest()
	)
	
	return not player.infected is None

def get_nearest_tasks(user, lat = None, lon = None):
	iteration = Iteration.objects.latest()
	
	player = Player.objects.get(
		user = user,
		iteration = iteration
	)
	
	if not lat or not lon:
		lat, lon = player.latitude, player.longitude
	
	tasks = Task.objects.filter(
		iteration = iteration,
		players__isnull = True
	).near(lat, lon).order_by('proximity')
	
	return dict({'near' : tasks })

def get_nearest_cure_centers(user, lat = None, lon = None):
	iteration = Iteration.objects.latest()
	
	player = Player.objects.get(
		user = user,
		iteration = iteration
	)
	
	if not lat or not lon:
		lat, lon = player.latitude, player.longitude
	
	cure_centers = Meetup.objects.filter(
		iteration = iteration,
		players__isnull = True
	).near(lat, lon)

	for center in cure_centers:
		if not player.infected is None:
			check_cure_proximity(player, center)
		else:
			break
	
	return dict({'near_centers' : cure_centers })

def get_lat_lon(request):
	latlon = list()
	if request.REQUEST.has_key('lat'):
		latlon.append( request.REQUEST['lat'] )
	if request.REQUEST.has_key('lon'):
		latlon.append( request.REQUEST['lon'] )
	
	if(len(latlon) != 2):
		return False
	
	player = Player.objects.get(
		user = request.user,
		iteration = Iteration.objects.latest()
	)
	player.latitude, player.longitude = latlon
	player.save()
	
	return True

def attempt_cure(user):
	player = Player.objects.get(
		user = user,
		iteration = Iteration.objects.latest()
	)
	cure_center = Meetup(iteration = player.iteration, latitude = player.latitude, longitude = player.longitude)
	cure_center.save()

	check_cure_proximity(player, cure_center)

def check_cure_proximity(player, meetup):

	if meetup.can_cure(player.latitude, player.longitude):
		player.infected = None
		player.save()

