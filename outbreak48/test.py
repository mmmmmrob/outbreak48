from functools import wraps
import string

auto_urls = dict()

class register_url(object):

    def __init__(self, url):
        urls[url] = self

    def __call__(self, f):
        def wrapped_f(*args):
            f(*args)
        self.wrap = wrapped_f
        return wrapped_f
    
    def call(self):
    	return self.wrap

@register_url("hello")
def sayHello(a1, a2, a3, a4):
    print 'sayHello arguments:', a1, a2, a3, a4

print urls
urls['hello'].call()()